﻿using System;
using System.Collections.Generic;

namespace stadier
{
    class Program
    {
        public static Stadier stadierN = new Stadier(1, "stadier1", "North", new List<int>() { 29, 30, 31, 32 }, new List<int>() { 1, 2, 3, 4 });
        public static Stadier stadierW = new Stadier(4, "stadier4", "West", new List<int>() {21,22,23,24}, new List<int>() { 25, 26, 27, 28 });
        public static Stadier stadierS = new Stadier(2, "stadier2", "South", new List<int>() { 13, 14, 15, 16 }, new List<int>() { 17, 18, 19, 20 } );
        public static Stadier stadierE = new Stadier(3, "stadier3", "East", new List<int>() { 5, 6, 7, 8 } , new List<int>() { 9, 10, 11, 12 });

        public static List<Stadier> stadiers = new List<Stadier>() { stadierN, stadierW, stadierS, stadierE };
        static void Main(string[] args)
        {

            Console.WriteLine("à quelle stadier êtes-vous ? [North,South,East,West]");
            string saisie = Console.ReadLine();

            if(saisie != "North" && saisie != "South" && saisie != "East" && saisie != "West")
            {
                Console.WriteLine("Il n'y a pas de stadier à cette position");
                Main(args);
            }

            Console.WriteLine("Quel est votre numéro de place ? [entre 1 et 32 incule]");
            int saisie2 = 0;

            try
            {
                saisie2 = Convert.ToInt32(Console.ReadLine());

            }catch(Exception e)
            {
                Console.WriteLine("Votre saisie ne représente pas une place valide [ entre 1 et 32]");
            }

            if (saisie2 < 1 || saisie2 > 32)
            {
                Console.WriteLine("cette place n'existe pas");
                Main(args);
            }

            string targetLocation = "";

            foreach (Stadier stadier in stadiers)
            {
                if(stadier.PlacesLeft.Contains(saisie2) || stadier.PlacesRight.Contains(saisie2))
                {
                    targetLocation = stadier.Location;
                }
            }

            if (targetLocation.Equals(saisie))
            {
                Console.WriteLine("Vous êtes à la bonne entrée");
            }
            else
            {

                switch (saisie)
                {
                    case "North":
                        if (stadierE.PlacesLeft.Contains(saisie2) || stadierE.PlacesRight.Contains(saisie2) || stadierS.PlacesLeft.Contains(saisie2))
                        {
                            Console.WriteLine("Allez à droit");
                        }
                        else
                        {
                            Console.WriteLine("Allez à gauche");
                        }
                        break;
                    case "East":
                        if (stadierS.PlacesLeft.Contains(saisie2) || stadierS.PlacesRight.Contains(saisie2) || stadierW.PlacesLeft.Contains(saisie2))
                        {
                            Console.WriteLine("Allez à droit");
                        }
                        else
                        {
                            Console.WriteLine("Allez à gauche");
                        }
                        break;
                    case "West":
                        if (stadierN.PlacesLeft.Contains(saisie2) || stadierN.PlacesRight.Contains(saisie2) || stadierE.PlacesLeft.Contains(saisie2))
                        {
                            Console.WriteLine("Allez à droit");
                        }
                        else
                        {
                            Console.WriteLine("Allez à gauche");
                        }
                        break;
                    case "South":
                        if (stadierW.PlacesLeft.Contains(saisie2) || stadierW.PlacesRight.Contains(saisie2) || stadierN.PlacesLeft.Contains(saisie2))
                        {
                            Console.WriteLine("Allez à droit");
                        }
                        else
                        {
                            Console.WriteLine("Allez à gauche");
                        }
                        break;

                }
                Main(args);
            }


                
        }
    }
}
