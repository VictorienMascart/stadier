﻿using System;
using System.Collections.Generic;
using System.Text;

namespace stadier
{
    class Stadier
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Location { get; set; }
        public List<int> PlacesLeft { get; set; }
        public List<int> PlacesRight { get; set; }

        public Stadier(int id, string name, string location, List<int> placesl, List<int> placesr)
        {
            Id = id;
            Name = name;
            Location = location;
            PlacesLeft = placesl;
            PlacesRight = placesr;
        }
    }
}
